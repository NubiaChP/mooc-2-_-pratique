# Accompagner l'individu et le collectif

Dans les modules précédents, nous avons créés des activités pour les élèves en détaillant la mise en oeuvre en classe. Il va s'agir dans ce troisième module de préciser comment accompagner chaque élève dans son apprentissage.

Qu'il soit en difficulté ou en avance, chaque élève est singulier et demande souvent de proposer des activités supplémentaires, de remédiation ou d'explorations complémentaires.

Une fois notre activité choisie, créée, sa mise en oeuvre soigneusement orchestrée, nous pouvons accompagner plus avant les élèves : ceux et celles qui ont pris de l'avance, ceux et celles qui semblent en diffculté, l'ensemble de la classe en proposant par exemple un réinvestissement des apprentissages lors d'un projet.

Cette partie se structure différement des précédentes, nous ne sommes plus dans une démarche de "modélisation" mais de propositions concrètes pour faire face aux défis usuels qui peuvent se poser. Par exemple :

- Proposer une activité de remédiation pour des élèves en difficulté
- Proposer une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance
- Proposer un projet en précisant :
    - les connaissances travaillées
    - le format pédagogique (durée, seul, binôme, groupe...)
    - les étayages pour assurer une progression des élèves
    - le calendrier (points d'étapes, rendus intermédiaires...)




## Objectifs

Les objectifs visés sont :
* A partir d'une activité existante :
  * Identifier et critiquer le scénario d'une séquence
  * Scénariser une séquence existante
* A partir de l'activité que vous avez _créée_ en 2.1 : **scénariser entièrement une séance**, en précisant notamment :
    * les différentes phases, durée,
    * l'organisation du tableau, écran, papier
    * l'organisation pédagogique (seul, binôme, groupe...)
    * les attendus
    * les interactions
    * l'activité du professeur
    * la place de la trace écrite


## Boîte à outils

Pour vous préparer vous aurez à votre disposition : 

- La quatrième partie, du livre "Enseigner l'Informatique" de Werner Hartmann, suivi du quiz-bloc2 n°3

- La cinquième partie, du livre "Enseigner l'Informatique" de Werner Hartmann, suivi du quiz-bloc2 n°4

- 4 ressources de collégues pour vous inspirer et discuter sur le forum.

- La to do liste de fin, avec les actions à réaliser pour répondre aux objectifs ET pour bien se préparer pour l'évaluation par les pairs.

Et bien sur la [communauté d’apprentissage et de pratique grâce au forum](https://mooc-forums.inria.fr/moocnsi/c/mooc-2/accompagner/194) qui fait converger le forum de la communauté CAI (Communauté d'Apprentissage de l'Informatique) et les forums des Moocs NSI.



## Travail demandé

Plusieurs types d'activité peuvent constituer le contenu de ce troisième module. On pourra par exemple :

- Proposer une activité de remédiation pour des élèves en difficulté
- Proposer une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance
- Proposer un projet en précisant :
    - les connaissances travaillées
    - le format pédagogique (durée, seul, binôme, groupe...)
    - les étayages pour assurer une progression des élèves
    - le calendrier (points d'étapes, rendus intermédiaires...)
- Proposer une activité pour la découverte ou la consolidation d'un concept qui autorise des cheminements différents pour atteindre un même objectif (voir par exemple l'activité sur les types abstraits de données).

L'activité produite pour ce module pourra soit faire référence à des activités proposées aux modules 1 et 2 soit être sans lien. Dans tous les cas, il faudra bien préciser non seulement la nature de l'activité mais également son contexte.

################ Hartmann ##################

Nous vous proposons de commencer ce module par la lecture de la [quatrième partie](lien.vers.le.pdf) de l'ouvrage de Hartmann _etal_. Les auteurs nous présentent plusieurs méthodes d'enseignement : la pédagogie expérientielle, le travail de groupe, les programmes dirigés, les apprentissages par la découverte ou encore la pédagogie de projet. 

Autant de méthodes qui peuvent vous donner des idées pour proposer, en complément de votre activité initiale, une autre activité, individuelle ou collective.

Cette lecture pourra se compléter de la [cinquième partie](lien.vers.le.pdf) qui aborde les techniques d'enseignement. Parmi ces techniques l'une consiste à rendre concrètes des notions abstraites par le jeu des représentations énactiques, iconiques et symboliques.
