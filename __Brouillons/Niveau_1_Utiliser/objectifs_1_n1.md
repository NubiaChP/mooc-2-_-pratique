## 1. Penser - Concevoir - Élaborer

### Niveau 1 -- Utiliser

- Identifier les objectifs (connaissances et compétences) d'une activité
- Identifier les pré-requis d'une activité
- Juger si une activité est appropriée ou pas 
- Adapter une activité
